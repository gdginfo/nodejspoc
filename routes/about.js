var express = require('express');
var router = express.Router();

router.get('/', function(req,res){

	if((!(req.query || {}).name) && (!(req.query || {}).title)){
		
		res.render('about', { title: 'About page',  description: 'This is the about page.'});
	}else{
		
		//You can retrieve those variables from the url for instance /about?name=Steeve Henri&title=Développeur NodeJS.
		var author = req.query.name,
			authorTitle = req.query.title;
		
		console.log(author + "-" + authorTitle);
		res.render('about', {title: 'About ' + author, description: 'The title of this author is: ' + authorTitle})
	}
});

module.exports = router;