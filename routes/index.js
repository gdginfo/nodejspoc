var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express',  description: 'This is a description made for the index page only.'});  
});

module.exports = router;
